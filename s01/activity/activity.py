name = "Romhert"
age = 13
occupation = "System Developer"
movie = "Zom 100: Bucket List of the Dead"
rating = 33.33

print(f"I am {name}, and my age is {age} years old, I work as a {occupation}, and my rating for {movie} is {rating} %")

num1 = 3
num2 = 50
num3 = 4

print(num1 * num2)
print(num1 < num3)
num2 = num2 + num3