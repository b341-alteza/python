from abc import ABC, abstractmethod

class Animal(ABC):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass

    @abstractmethod
    def call(self):
        pass

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__(name, breed, age)

    def eat(self, food):
        return f"{self._name} the cat is eating {food}"

    def make_sound(self):
        return "Meow!"

    def call(self):
        return f"{self._name}, ming ming ming!"

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__(name, breed, age)

    def eat(self, food):
        return f"{self._name} the dog is eating {food}"

    def make_sound(self):
        return "Woof!"

    def call(self):
        return f"{self._name}, come here dog!"

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

cat = Cat("Draken", "Savannah", 5)
dog = Dog("Flame", "Alaskan Malamute", 3)

print(cat.eat("tinik"))  
print(cat.make_sound()) 
print(cat.call())

print(dog.eat("buto"))  
print(dog.make_sound())  
print(dog.call())  
