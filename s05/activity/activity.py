from abc import ABC, abstractmethod

class Person(ABC):
    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    @abstractmethod
    def getFullName(self):
        pass

    @abstractmethod
    def addRequest(self, request):
        pass

    @abstractmethod
    def checkRequest(self, request):
        pass

    @abstractmethod
    def addUser(self, user):
        pass

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self):
        return f"Request has been added"

    def checkRequest(self, request):
        pass

    def addUser(self, user):
        return "Member has been added"

class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        self._members = []

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self, request):
        return f"Request has been added"

    def checkRequest(self, request):
        pass

    def addUser(self, user):
        pass

    def addMember(self, employee):
        self._members.append(employee)
        return "Member has been added"

    def get_members(self):
        return self._members



class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self, request):
        return f"Request has been added"

    def checkRequest(self, request):
        pass

    def addUser(self):
        return "User has been added"

class Request:
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "Open"

    def updateRequest(self):
        self._status = "Updated"
        return f"Request {self._name} has been updated"

    def closeRequest(self):
        self._status = "Closed"
        return f"Request {self._name} has been closed"

    def cancelRequest(self):
        self._status = "Cancelled"
        return f"Request {self._name} has been cancelled"

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_requester(self):
        return self._requester

    def set_requester(self, requester):
        self._requester = requester

    def get_dateRequested(self):
        return self._dateRequested

    def set_dateRequested(self, dateRequested):
        self._dateRequested = dateRequested

    def get_status(self):
        return self._status

    def set_status(self, status):
        self._status = status


employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():    
    print(indiv_emp.getFullName())
    assert admin1.addUser() == "User has been added"
    
req2.set_status("closed")
print(req2.closeRequest())